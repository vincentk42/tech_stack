export default (state = null, action ) => {
    /*always called with state object(which is the state that is returned the last time
         the reducer was executed)

         
    */
    // console.log(action) 
    // return null;

    switch(action.type){
        case 'select_library':
            return action.payload
        default: 
            return state;
    }
};