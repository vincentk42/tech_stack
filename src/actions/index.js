export const selectLibrary = (libraryId) => {  //this is an actionCreator
    return {
        type: 'select_library',   
        payload: libraryId,
        
    };
};