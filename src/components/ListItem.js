import React, { Component } from 'react';
import { Text, TouchableWithoutFeedback, View, LayoutAnimation, NativeModules } from 'react-native';
import { connect } from 'react-redux';
import {CardSection} from './common/CardSection';
import * as actions from '../actions';
const { UIManager } = NativeModules
UIManager.setLayoutAnimationEnabledExperimental
    && UIManager.setLayoutAnimationEnabledExperimental(true)

class ListItem extends Component {

    componentWillUpdate() {
        LayoutAnimation.spring();
    }
    renderDescription() {
        const { library, expanded } = this.props;
     
        if(expanded) {
            return (
                <CardSection>
                    <Text style={{flex:1}}>{library.description}</Text>
                </CardSection>
            )
        }
    }

    render() {

        const { titleStyle } = styles;
        
        const { id, title } = this.props.library;

        return(
            <TouchableWithoutFeedback
                onPress={()=> this.props.selectLibrary(id)}
            >
                <View>
                    <CardSection>
                        <Text
                            style={titleStyle}
                        >{title}</Text>
                    </CardSection>
                    {this.renderDescription()}
                </View>
            </TouchableWithoutFeedback>
        );
    }
}

const styles = {
    titleStyle: {
        fontSize: 18,
        paddingLeft: 15,
    }
}

const mapStateToProps = (state, ownProps)  => {  
    const expanded = state.selectedLibraryId === ownProps.library.id;

    /*NOTE: THIS WILL ALWAYS RETURN AN OBJECT, also, only called with the state object
    remember always set this equal to state first otherwise you will get an error that says
    "state is undefined"*/
    return {expanded}
}

export default connect(mapStateToProps, actions)(ListItem);
/*NOTE when using connect, the first property must be mapStateToProps function, it can BE Null
the purpose of the second parameter is to bind actioncreators to this component 
this does two things:  first it turns the selectLibrary function(actionCreator) into something that when called
the return action will be automatically dispatched to the redux store 

the second thing it does is takes all the actions inside the object and passes them to the component as props
You'll notice that whenever we run the connect function/helper it modifies what data is shows up in our ListItem as props

*/