 import React, { Component } from 'react';
import { View, Text, ListView } from 'react-native';
import { connect } from 'react-redux';
import ListItem from './ListItem';


class LibraryList extends Component {
    componentWillMount() {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2)=> r1 !== r2 
        })
    /*NOTE: this is merely a wrapper for data, in this case(this.props.libraries)
    
    we actually want rendered in ListView*/
    this.dataSource = ds.cloneWithRows(this.props.libraries)
    }

    renderRow(library) {
        return <ListItem library={library} />;
      }

    render(){
        return(
            <ListView
                dataSource={this.dataSource}
                renderRow={this.renderRow}  /*notice, this is a helper method*/
            />
        )
    }
}


/*

take the global state object(which sits inside the redux store)

*/
const mapStateToProps = state => {
    return { libraries: state.libraries} 
    /*remember that mapStateToProps returns an object which shows up as props in LibraryList
    Also remember that the key (libraries can be called anything) HOWEVER, state.libraries must be called
    that due to what we created inside the combinedReducer method in index.js
    */
};

export default connect(mapStateToProps) (LibraryList);

/*
when connect is called, it returns another function, then immediately we call that function with
LibraryList..this is a 2 step process




*/